docker run -it --rm \
  --network=shadowing_default \
  apachebench -n 10000 -c 30  http://traefik/

docker run -it \
  -v $PWD/config/taurus/config.yml:/tmp/config.yml \
  --network=shadowing_default \
  blazemeter/taurus:1.14.0 /tmp/config.yml
  
docker run -it \
  -v $PWD/traefik/scenarios/05_metrics/config/taurus/config.yml:/tmp/config.yml \
  --network=05_metrics_default \
  blazemeter/taurus:1.14.0 /tmp/config.yml

docker run -it \
  -v $PWD/config/taurus/config.yml:/tmp/config.yml \
  --network=canarying_default \
  blazemeter/taurus:1.14.0 /tmp/config.yml
  
docker run -it \
  -v $PWD/config/consul/:/tmp/consul \
  --network=canarying_default \
  consul kv export -http-addr=http://consul:8500 > canarying.json
  
docker run -it \
  -v $PWD/config/consul/:/tmp/consul \
  --network=shadowing_default \
  consul kv export -http-addr=http://consul:8500 > shadowing.json

curl --verbose --header 'Test: v2' 'http://localhost'
curl --verbose --header 'Host: whoamiv1-shadowing' 'http://127.0.0.1'
curl --verbose --header 'Host: whoamiv1-shadowing' 'http://127.0.0.1:8181'
