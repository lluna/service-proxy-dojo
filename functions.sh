up() {
  SCENARIO=$1
  DETACH=${2:-}
  case $SCENARIO in
  base)
    compose_action 00_base up $DETACH
    ;;
  simple)
    compose_action 01_simple up $DETACH
    ;;
  multiple)
    compose_action 02_multiple up $DETACH
    ;;
  routers)
    compose_action 03_routers up $DETACH
    ;;
  integration)
    compose_action 04_integration up $DETACH
    ;;
  metrics)
    compose_action 05_metrics up $DETACH
    ;;
  shadowing)
    docker-compose -p shadowing -f ./traefik/scenarios/06_shadowing/docker-compose.yml up $DETACH
    ;;
  canarying)
    docker-compose -p canarying -f ./traefik/scenarios/07_canarying/docker-compose.yml up $DETACH
    ;;
  *)
    echo "El escenario [$SCENARIO] no existe"
    ;;
esac

}

down() {
  SCENARIO=$1
  case $SCENARIO in
  base)
    compose_action 00_base down
    ;;
  simple)
    compose_action 01_simple down
    ;;
  multiple)
    compose_action 02_multiple down
    ;;
  routers)
    compose_action 03_routers down
    ;;
  integration)
    compose_action 04_integration down
    ;;
  metrics)
    compose_action 05_metrics down
    ;;
  shadowing)
    docker-compose -p shadowing -f ./traefik/scenarios/06_shadowing/docker-compose.yml down $DETACH
    ;;
  canarying)
    docker-compose -p canarying -f ./traefik/scenarios/07_canarying/docker-compose.yml down $DETACH
    ;;
  *)
    echo "El escenario [$SCENARIO] no existe"
    ;;
esac

}

compose_action() {
  FOLDER=$1
  ACTION=$2
  DETACH=${3:-}
  echo "[${FOLDER}] $ACTION ... "
  docker-compose -p $FOLDER -f ./traefik/scenarios/${FOLDER}/docker-compose.yml $ACTION $DETACH
}
