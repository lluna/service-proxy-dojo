setup: build-executor
hajime: start-executor

build-executor:
	docker build -t executor .

prepare:
	make -C whoami image
	docker pull traefik:v2.1
	docker pull google/cadvisor
	docker pull prom/prometheus:v2.9.2
	docker pull grafana/grafana:5.1.0
	docker pull gliderlabs/registrator:master
	docker pull consul
	docker pull blazemeter/taurus:1.14.0
