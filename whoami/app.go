package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
  "math/rand"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

const (
	_        = iota
	KB int64 = 1 << (10 * iota)
	MB
	GB
	TB
)

var cert string
var key string
var port string

func init() {
	flag.StringVar(&cert, "cert", "", "give me a certificate")
	flag.StringVar(&key, "key", "", "give me a key")
	flag.StringVar(&port, "port", "80", "give me a port number")
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
  rand.Seed(time.Now().UnixNano())
	flag.Parse()

	r := mux.NewRouter()
	r.HandleFunc("/forward/{service}/{port}/{path}", forwardHandler)
	// r.HandleFunc("/", whoamiHandler)
	r.HandleFunc("/random500", randomFiveHundredHandler)
  r.PathPrefix("/").HandlerFunc(whoamiHandler)

	// http.HandleFunc("/data", dataHandler)
	// http.HandleFunc("/echo", echoHandler)
	// http.HandleFunc("/bench", benchHandler)
	//
	// http.HandleFunc("/api", apiHandler)
	// http.HandleFunc("/health", healthHandler)
	http.Handle("/", r)

	fmt.Println("Starting up on port " + port)

	if len(cert) > 0 && len(key) > 0 {
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func benchHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprint(w, "1")
}

func echoHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			return
		}

		printBinary(p)
		err = conn.WriteMessage(messageType, p)
		if err != nil {
			return
		}
	}
}

func printBinary(s []byte) {
	fmt.Printf("Received b:")
	for n := 0; n < len(s); n++ {
		fmt.Printf("%d,", s[n])
	}
	fmt.Printf("\n")
}

func dataHandler(w http.ResponseWriter, r *http.Request) {
	u, _ := url.Parse(r.URL.String())
	queryParams := u.Query()

	size, err := strconv.ParseInt(queryParams.Get("size"), 10, 64)
	if err != nil {
		size = 1
	}
	if size < 0 {
		size = 0
	}

	unit := queryParams.Get("unit")
	switch strings.ToLower(unit) {
	case "kb":
		size = size * KB
	case "mb":
		size = size * MB
	case "gb":
		size = size * GB
	case "tb":
		size = size * TB
	}

	attachment, err := strconv.ParseBool(queryParams.Get("attachment"))
	if err != nil {
		attachment = false
	}

	content := fillContent(size)

	if attachment {
		w.Header().Add("Content-Disposition", "Attachment")
		http.ServeContent(w, r, "data.txt", time.Now(), content)
		return
	}

	if _, err := io.Copy(w, content); err != nil {
		http.Error(w, "Uppps", http.StatusInternalServerError)
		return
	}
}

func whoamiHandler(w http.ResponseWriter, req *http.Request) {

	log.Println(fmt.Sprintf("%v %v %v", req.Method, req.URL, req.Proto))
  
  randomRangeRaw := os.Getenv("RANDOM_500")
  
  if randomRangeRaw != "" {
    randomRange, err := strconv.ParseFloat(os.Getenv("RANDOM_500"), 64); 
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
  
    if randomRange > 0 && rand.Float64() < randomRange {
      log.Println("Throwing an random 500 error!")
      http.Error(w, "uppps", http.StatusInternalServerError)
      return
	  }
  }

	u, _ := url.Parse(req.URL.String())
	wait := u.Query().Get("wait")
	if len(wait) > 0 {
		duration, err := time.ParseDuration(wait)
		if err == nil {
			time.Sleep(duration)
		}
	}

	hostname, _ := os.Hostname()
	fmt.Fprintln(w, "Hostname:", hostname)

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			fmt.Fprintln(w, "IP:", ip)
		}
	}

	if err := req.Write(w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func apiHandler(w http.ResponseWriter, req *http.Request) {
	hostname, _ := os.Hostname()

	data := struct {
		Hostname string      `json:"hostname,omitempty"`
		IP       []string    `json:"ip,omitempty"`
		Headers  http.Header `json:"headers,omitempty"`
		URL      string      `json:"url,omitempty"`
		Host     string      `json:"host,omitempty"`
		Method   string      `json:"method,omitempty"`
	}{
		Hostname: hostname,
		IP:       []string{},
		Headers:  req.Header,
		URL:      req.URL.RequestURI(),
		Host:     req.Host,
		Method:   req.Method,
	}

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			data.IP = append(data.IP, ip.String())
		}
	}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

type healthState struct {
	StatusCode int
}

var currentHealthState = healthState{http.StatusOK}
var mutexHealthState = &sync.RWMutex{}

func healthHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		var statusCode int

		if err := json.NewDecoder(req.Body).Decode(&statusCode); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		fmt.Printf("Update health check status code [%d]\n", statusCode)

		mutexHealthState.Lock()
		defer mutexHealthState.Unlock()
		currentHealthState.StatusCode = statusCode
	} else {
		mutexHealthState.RLock()
		defer mutexHealthState.RUnlock()
		w.WriteHeader(currentHealthState.StatusCode)
	}
}

func forwardHandler(w http.ResponseWriter, req *http.Request) {

	log.Println(fmt.Sprintf("%v %v %v", req.Method, req.URL, req.Proto))

	hostname, _ := os.Hostname()
	fmt.Fprintln(w, "Hostname:", hostname)

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			fmt.Fprintln(w, "IP:", ip)
		}
	}

	vars := mux.Vars(req)
	forwarded := &url.URL{
		Scheme: "http",
		Host:   vars["service"] + ":" + vars["port"],
		Path:   vars["path"],
	}

	resp := makeRequest(forwarded.String())

	if err := req.Write(w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintln(w, "-------------")
	fmt.Fprintln(w, "Forwarded to :", forwarded.String())
	fmt.Fprintln(w, resp)

}

func randomFiveHundredHandler(w http.ResponseWriter, req *http.Request) {

	log.Println(fmt.Sprintf("%v %v %v", req.Method, req.URL, req.Proto))

	if rand.Float32() < 0.5 {
    log.Println("Throwing and 500 error!")
		http.Error(w, "uppps", http.StatusInternalServerError)
		return
	}
  
  hostname, _ := os.Hostname()
	fmt.Fprintln(w, "Hostname:", hostname)

}

func makeRequest(uri string) string {
	resp, err := http.Get(uri)
	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return string(body)
}

func fillContent(length int64) io.ReadSeeker {
	charset := "-ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, length, length)

	for i := range b {
		b[i] = charset[i%len(charset)]
	}

	if length > 0 {
		b[0] = '|'
		b[length-1] = '|'
	}

	return bytes.NewReader(b)
}
