# {{ timestamp }}
{{ if and (keyExists "traefik/shadowing/enabled") (key "traefik/shadowing/enabled" | parseBool )}}
[http.routers]
  [http.routers.mirrored-whoami]
    rule = "PathPrefix(`/`)"
    service = "mirrored-whoami"
    entrypoints= ["web"]

[http.services]
  [http.services.mirrored-whoami]
  {{ range service "whoami" }}
    {{if in .Tags "shadow"}}
    [[http.services.mirrored-whoami.mirroring.mirrors]]
      name = "whoami{{.ServiceMeta.version}}@docker"
      percent = {{keyOrDefault "traefik/shadowing/how_much" "10" }}
    {{ else }}
    [http.services.mirrored-whoami.mirroring]
      service = "whoami{{.ServiceMeta.version}}@docker"
    {{ end }}
  {{ end }}
{{ end }}
