# {{ timestamp }}
{{ if and (keyExists "traefik/canarying/enabled") (key "traefik/canarying/enabled" | parseBool )}}
[http.routers]
  [http.routers.whoami]
    rule = "PathPrefix(`/`)"
    service = "whoami"
    entrypoints= ["web"]
    
[http.services]
  [http.services.whoami]
    [[http.services.whoami.weighted.services]]
      name = "whoamiv1@docker"
      weight = {{ keyOrDefault "traefik/canarying/v1" "100" }}
    [[http.services.whoami.weighted.services]]
      name = "whoamiv2@docker"
      weight = {{ keyOrDefault "traefik/canarying/v2" "0" }}

{{ end }}
