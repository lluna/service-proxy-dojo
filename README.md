docker build . -t executor

docker run -it --rm \
  -v $PWD:/demo \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -w /demo \
  executor
